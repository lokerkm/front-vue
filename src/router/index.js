import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import Main from '@/components/Main'
import Home from '@/components/users/Main'
import Users from '@/components/users/List'
import Species from '@/components/swapi/species/Main'
import Specie from '@/components/swapi/species/Specie'
import Vehicles from '@/components/swapi/vehicles/Main'
import Vehicle from '@/components/swapi/vehicles/Vehicle'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main,
    },
    {
      path: '/home',
      name: 'Home',
      component: Home,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/users',
      name: 'Users',
      component: Users,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/swapi/species',
      name: 'Species',
      component: Species,
    },
    {
      path: '/swapi/specie',
      name: 'Specie',
      props: true,
      component: Specie,
    },
    {
      path: '/swapi/vehicle',
      name: 'Vehicle',
      props: true,
      component: Vehicle,
    },
    {
      path: '/swapi/vehicles',
      name: 'Vehicles',
      component: Vehicles,
    },
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next()
      return
    }
    next('/')
    return
  } else {
    next()
    return
  }
})

export default router
