
import Http from './Http';


export const getAllSpecies = (page) => {
  return  Http.get('/species', { params: {page} });
};
export const getSpecie = (id) => {
  return  Http.get('/species/'+id);
};

export const getPeople = (id) => {
  return  Http.get('/people/'+id);
}
export const getFilm = (id) => {
  return  Http.get('/films/'+id);
}
export const getVehicle = (id) => {
  return  Http.get('/vehicles/'+id);
}

export const getAllVehicles = (page) => {
  return  Http.get('/vehicles', { params: {page} });
};


